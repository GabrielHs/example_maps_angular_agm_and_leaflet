import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaMunicipioComponent } from './components/mapa-municipio/mapa-municipio.component';
import { MapaMunicipio2Component } from './components/mapa-municipio2/mapa-municipio2.component';


const routes: Routes = [
  { path: 'municipio', component: MapaMunicipioComponent },
  { path: 'municipio2', component: MapaMunicipio2Component },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
