import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'coordsMunicipio'
})
export class CoordsMunicipioPipe implements PipeTransform {

  transform(value: any, tipo: string): number {
    const coordString = JSON.parse(value)
    if (tipo === 'lat') {
      return coordString[0];
    } else {
      return coordString[1];
    }

  }

}
