import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'iconsMaps'
})
export class IconsMapsPipe implements PipeTransform {

  transform(situacao: any, ...args: unknown[]): any {
    let icon = {
      url: 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg',
      scaledSize: {
        width: 25,
        height: 25
      }
    }
    if (situacao < 2) {
      icon.url =  "https://cadastro.provedor.space/images/m7.png";
    } else if (situacao < 5) {
      icon.url =  "https://cadastro.provedor.space/images/m2.png";
    } else {
      icon.url =  "https://cadastro.provedor.space/images/m6.png";
    }
    return icon;
  }

}
