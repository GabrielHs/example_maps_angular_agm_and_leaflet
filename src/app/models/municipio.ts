export interface Municipio {

    name: string;

    coordinates: Coordinates;

    population: string;

    cases_s: string;

    cases_d: string;

    cases_c: string;

    deaths: string;

    

}

export interface Coordinates {
    lat: string;
    lng: string;
}
