import { Municipio } from './../../models/municipio';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MunicipioService {

  constructor(private http: HttpClient) { }

  baseUrl = `https://mapa-covid19.saude.ma.gov.br/data.php?type=ma&_=1587329719431`


  getAll(): Observable<Municipio[]> {
    return this.http.get<Municipio[]>(`${this.baseUrl}`)
  }


  
}
