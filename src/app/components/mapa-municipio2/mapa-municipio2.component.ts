import { Municipio } from 'src/app/models/municipio';
import { MunicipioService } from './../../services/mapa/municipio.service';
import { Component, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { latLng, MapOptions, tileLayer, Map, Marker, icon } from 'leaflet';
import { animation } from '@angular/animations';

@Component({
  selector: 'app-mapa-municipio2',
  templateUrl: './mapa-municipio2.component.html',
  styleUrls: ['./mapa-municipio2.component.css']
})
export class MapaMunicipio2Component implements OnInit {

  public title = 'My first Leaflet project';

  public map: Map;
  public mapOptions: MapOptions;
  public listaMunicipios: Municipio[];
  public layers: L.Layer;
  public iconCustom: boolean = false;
  public mcg: any

  constructor(private municipioService: MunicipioService) {
  }

  ngOnInit(): void {
    this.carregaMunicipios()

    this.initializeMapOptions();
  }

  carregaMunicipios() {
    this.municipioService.getAll().subscribe(
      (municipio: Municipio[]) => { // sucesso
        this.listaMunicipios = municipio
        this.addSampleMarker()

      },
      (erro: any) => { // erro
        console.error(erro)
      }
    )
  }

  onMapReady(map: Map) {
    this.map = map;

  }

  private initializeMapOptions() {
    this.mapOptions = {
      center: latLng(-14.235004, -51.92528),
      zoom: 4,
      layers: [
        L.tileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="//openstreetmap.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
        })
      ],
    };
  }

  public addSampleMarker() {


    // utilizando https://github.com/Leaflet/Leaflet.markercluster
    this.mcg = L.markerClusterGroup({
      // chunkedLoading: true,
      // //singleMarkerMode: true,
      // spiderfyOnMaxZoom: false,
      // showCoverageOnHover: false,
      // zoomToBoundsOnClick: true
      
      
    });
    for (const item of this.listaMunicipios) {

      const coodsString = item.coordinates

      let lat = this.convertStringJson(coodsString, 'lat');
      let lng = this.convertStringJson(coodsString, 'lng')



      let tittleMap = `
      <div style="text-align:center">
          <div>
            <b>${item.name}</b>
          </div>
          <br>
          <div>Confirmados: ${item.cases_c} | Descartados: ${item.cases_d} | Óbitos: ${item.deaths}
          </div>
      </div>`

      let myIcon = L.icon({
        iconUrl: `${this.iconCustom ? 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg' : 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`,
        // shadowUrl: `${this.iconCustom ? 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg' : 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`,

        iconSize: [25, 41], // size of the icon
        // shadowSize:   [50, 64], // size of the shadow
        // iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
        // shadowAnchor: [4, 62],  // the same for the shadow
        // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      });
      var maker = L.marker(new L.LatLng(lat, lng), { title: tittleMap, icon: myIcon });

      maker.bindPopup(tittleMap);
      this.mcg.addLayer(maker);
      // let myIcon = L.icon({
      //   iconUrl: `${this.iconCustom ? 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg' : 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`,
      //   // shadowUrl: `${this.iconCustom ? 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg' : 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`,

      //   iconSize: [25, 41], // size of the icon
      //   // shadowSize:   [50, 64], // size of the shadow
      //   // iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      //   // shadowAnchor: [4, 62],  // the same for the shadow
      //   // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      // });

      // markers.addLayer(L.marker(new L.LatLng(lat, lng), { icon: myIcon }));
      // markers.bindPopup
      //   (`
      //           <div style="text-align:center">
      //               <div>
      //                 <b>${item.name}</b>
      //               </div>
      //               <br>
      //               <div>Confirmados: ${item.cases_c} | Descartados: ${item.cases_d} | Óbitos: ${item.deaths}
      //               </div>
      //           </div>
      //  `)
      // this.map.addLayer(markers);
      // const marker = new Marker([lat, lng])
      //   .setIcon(
      //     icon({
      //       iconSize: [25, 41],
      //       iconAnchor: [13, 41],
      //       iconUrl: `${this.iconCustom ? 'https://www.flaticon.com/svg/static/icons/svg/2928/2928883.svg' : 'https://unpkg.com/leaflet@1.6.0/dist/images/marker-icon.png'}`
      //     }));
      // marker.bindPopup
      // (`
      //           <div style="text-align:center">
      //               <div>
      //                 <b>${item.name}</b>
      //               </div>
      //               <br>
      //               <div>Confirmados: ${item.cases_c} | Descartados: ${item.cases_d } | Óbitos: ${item.deaths}
      //               </div>
      //           </div>
      //  `)
      // marker.addTo(this.map);

    }
    this.map.addLayer(this.mcg)
  }



  convertStringJson(latlng: any, tipo: string): any {
    const coordString = JSON.parse(latlng)
    if (tipo === 'lat') {
      return coordString[0];
    } else {
      return coordString[1];
    }
  }

}
