import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaMunicipio2Component } from './mapa-municipio2.component';

describe('MapaMunicipio2Component', () => {
  let component: MapaMunicipio2Component;
  let fixture: ComponentFixture<MapaMunicipio2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaMunicipio2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaMunicipio2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
