import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaMunicipioComponent } from './mapa-municipio.component';

describe('MapaMunicipioComponent', () => {
  let component: MapaMunicipioComponent;
  let fixture: ComponentFixture<MapaMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
