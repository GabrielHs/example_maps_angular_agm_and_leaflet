import { MunicipioService } from './../../services/mapa/municipio.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Municipio } from 'src/app/models/municipio';
import { log } from 'console';
import { AgmMap } from '@agm/core';

@Component({
  selector: 'app-mapa-municipio',
  templateUrl: './mapa-municipio.component.html',
  styleUrls: ['./mapa-municipio.component.css']
})
export class MapaMunicipioComponent implements OnInit {

  public title = 'My first AGM project';

  public listaMunicipios: Municipio[]

  

 
  @ViewChild('AgmMap') agmMap: AgmMap;
  constructor(private municipioService: MunicipioService) { }

  ngOnInit(): void {
    this.carregaMunicipios()
  }


  carregaMunicipios() {
    this.municipioService.getAll().subscribe(
      (municipio: Municipio[]) => { // sucesso
        this.listaMunicipios = municipio

      },
      (erro: any) => { // erro
        console.error(erro)
      }
    )
  }

}
