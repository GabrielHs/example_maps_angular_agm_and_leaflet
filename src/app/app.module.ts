import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapaMunicipioComponent } from './components/mapa-municipio/mapa-municipio.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgmCoreModule } from '@agm/core';
import { CoordsMunicipioPipe } from './pipes/mapas/coords-municipio.pipe';
import { IconsMapsPipe } from './pipes/mapas/icons-maps.pipe';
import { MapaMunicipio2Component } from './components/mapa-municipio2/mapa-municipio2.component'
import {LeafletModule} from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster'


@NgModule({
  declarations: [
    AppComponent,
    MapaMunicipioComponent,
    CoordsMunicipioPipe,
    IconsMapsPipe,
    MapaMunicipio2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'SUA CHAVE DO GOOGLE MAPS'
    }),
    LeafletModule,
    LeafletMarkerClusterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
